import React from 'react';
import conditionIDToIconName from './iconUtils.js'

export default function Map(props) {
    let icon
    if (props.containsConditions) {
        icon = (id) => conditionIDToIconName(id)
    } else {
        icon = (_) => 'circle'
    }

    var latExtrema = [91.0, -91.0]
    var lonExtrema = [91.0, -91.0]

    const markers = props.points.map(point => {
        const lat = point.localisation.lat
        if (lat < latExtrema[0]) latExtrema[0] = lat
        if (lat > latExtrema[1]) latExtrema[1] = lat
        const lon = point.localisation.lon
        if (lon < lonExtrema[0]) lonExtrema[0] = lon
        if (lon > lonExtrema[1]) lonExtrema[1] = lon

        return [
            `lonlat:${lon},${lat}`,
            `icon:${icon(point.id)}`,
            'icontype:awesome',
            'iconsize:large',
            'type:awesome',
            'size:large',
            'color:#ff0000',
            'whitecircle:no'
        ].join(';')
    })

    markers.push([
        `lonlat:${(lonExtrema[0] + lonExtrema[1])/2},${(latExtrema[0] + latExtrema[1])/2}`,
        'icon:ear-listen',
        'icontype:awesome',
        'iconsize:large',
        'type:circle',
        'size:large',
        'color:#0000ff',
        'whitecircle:no'
    ].join(';'))

    console.log(latExtrema)
    console.log(lonExtrema)
    console.log(markers[markers.length - 1])
    /*
    props.points.forEach(point => {
        

        let icon = iconUtils.ConditionIDToIconName(point.id)
        markers.push(`lonlat:${point.lon},${point.lat};icon:${icon};type:circle;color:%23ffffff;size:xx-large;icontype:awesome;whitecircle:no`)
        markers.push(`;;;;;;`)
    });
    */
    const { innerWidth: width, innerHeight: height } = window;

    const url = new URL("https://maps.geoapify.com/v1/staticmap/")
    url.searchParams.append("style", "osm-bright-grey")
    url.searchParams.append("width", width)
    url.searchParams.append("height", height)
    url.searchParams.append("apiKey","60675623175b441fb58720afc747d4ee")
    url.searchParams.append("marker", markers.join('|'))

    console.log(url.href)
    return(
        <img src={url.href} alt="map"/>
    )
}