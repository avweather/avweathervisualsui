import './App.css';
import useFetch from 'use-http'
import { useState, useEffect } from 'react';
import Map from './Map'

function App() {
  const [geoframes, setGeoframes] = useState()
  const [report, setReport] = useState()

  const { get, patch, response, loading} = useFetch('http://localhost:8081')

  useEffect(() => {
    (async () => {
      const report = await get('/poll?condition-format=avweather')
      if (response.ok) {
        setReport(report)
      }

      const geoframes = await get('/geoframes')
      if (response.ok) {
        setGeoframes(geoframes)
      }
    })()
  }, [])

  async function changeCurrentGeoframe(id) {
    await patch('/geoframe', { "geoframeID": id })
    if (response.ok) {
      window.location.reload(true);
    } 
  }
  
  let map = null
  if (report !== undefined) {
    map = <Map points={ report.conditions } containsConditions='true' />
  }

  return (
    <>
      { loading && 'Loading ...'}

      <ul class="geoframe-list">
        {geoframes?.map(geoframe => {
          if (geoframe.id === report?.geoframeID) {
            return <li key={geoframe.id}><b>{geoframe.name}</b></li>
          } else {
            return (
              <li key={geoframe.id}>
                {geoframe.name}
                <button onClick={() => changeCurrentGeoframe(geoframe.id)}>Set</button>
              </li>
            ) 
          }
          
        })}
      </ul>
      {map}
    </>
  )
}

export default App;
