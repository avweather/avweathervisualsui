export default function conditionIDToIconName(id) {
    switch (id) {
        case 1:
            return "cloud-rain"
        case 2:
            return "cloud-showers-heavy"
        case 3:
        case 4: 
        case 5:
            return "cloud-bolt"
        case 6:
        case 7:
            return "snowflake"
        case 8:
            return "smog"
        case 9:
            return "wind"
        case 10:
            return "sun"
        case 11:
            return "cloud"
        case undefined:
        default:
            return "question"
    }
}